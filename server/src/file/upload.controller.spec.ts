import { Test, TestingModule } from '@nestjs/testing';
import { UploadController } from './upload.controller';
import { FileService } from './file.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { File } from './file.entity';

describe('Upload Controller', () => {
  let controller: UploadController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UploadController],
      providers: [
        FileService,
        {
          provide: getRepositoryToken(File),
          useValue: () => {},
        },
      ],
    }).compile();

    controller = module.get<UploadController>(UploadController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
