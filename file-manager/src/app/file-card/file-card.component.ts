import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-file-card',
  templateUrl: './file-card.component.html',
  styleUrls: ['./file-card.component.scss']
})
export class FileCardComponent implements OnInit {

  @Input() file;
  @Input() add = false;

  constructor() {
  }

  ngOnInit(): void {
  }

}
