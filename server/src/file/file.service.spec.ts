import { Test, TestingModule } from '@nestjs/testing';
import { FileService } from './file.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { File } from './file.entity';

describe('FileService', () => {
  let service: FileService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FileService,
        {
          provide: getRepositoryToken(File),
          useValue: () => {},
        },
      ],
    }).compile();

    service = module.get<FileService>(FileService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return a list of files files', async () => {
      const result: File[] = [
        {
          destination: '',
          encoding: '',
          filename: '',
          fieldname: '',
          id: 1,
          mimetype: '',
          originalname: '',
          path: '',
          size: 2,
        },
        {
          destination: '',
          encoding: '',
          filename: '',
          fieldname: '',
          id: 1,
          mimetype: '',
          originalname: '',
          path: '',
          size: 2,
        },
      ];
      jest
        .spyOn(service, 'getAllFiles')
        .mockImplementation(() => Promise.resolve(result));

      expect(await service.getAllFiles()).toBe(result);
    });
  });
});
