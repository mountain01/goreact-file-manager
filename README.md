# File Manager

## Installation

```bash
$ git clone https://gitlab.com/mountain01/goreact-file-manager.git
```

 ## Running app

 ```shell
 $ docker-compose up -d
 ```

 ## Testing

You will need to run tests for each piece separately.

### Frontend
 ```bash
 # from root
 $ cd file-manager
 $ npm install # if you haven't done so already
 $ ng test
 ```

 ### Backend
 ```bash
 # from root
 $ cd server
 $ npm install # if you haven't done so already
 $ npm run test
 ```
