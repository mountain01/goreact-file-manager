import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { tap, map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';

const SERVER_URL = 'http://localhost:3000/uploads';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  filesBS = new BehaviorSubject<any>([]);

  files$ = this.filesBS.asObservable();

  addFiles(files): void {
    this.filesBS.next(this.filesBS.getValue().concat(files));
  }

  upload(formData): Observable<any> {
    return this.http
      .post(SERVER_URL, formData, {
        observe: 'events',
        reportProgress: true,
      });
  }

  getFiles(): void {
    this.http
      .get<any[]>(`${SERVER_URL}`)
      .pipe(
        map((files) => {
          return files.map((file) => ({ ...file, inProgress: false, progress: 100, name: file.originalname}));
        })
      )
      .subscribe((files) => this.addFiles(files));
  }
}
