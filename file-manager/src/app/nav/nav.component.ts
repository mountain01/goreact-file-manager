import { Component, ViewChild, ElementRef } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable, of } from 'rxjs';
import { map, shareReplay, catchError } from 'rxjs/operators';
import { ApiService } from '../services/api.service';
import { HttpEventType, HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss'],
})
export class NavComponent {
  @ViewChild('fileUpload') fileUpload: ElementRef<HTMLInputElement>;
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );
  tempFiles: any[] = [];
  files$ = this.api.files$;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private api: ApiService
  ) {
    this.api.getFiles();
  }

  addNewFile(): void {
    this.fileUpload.nativeElement.click();
  }

  uploadFile(file): void {
    this.api.addFiles(file);
    const formData = new FormData();
    formData.append('file', file.data);
    file.inProgress = true;
    this.api
      .upload(formData)
      .pipe(
        map((event) => {
          switch (event.type) {
            case HttpEventType.UploadProgress:
              file.progress = Math.round((event.loaded * 100) / event.total);
              break;
            case HttpEventType.Response:
              return event;
          }
        }),
        catchError((error: HttpErrorResponse) => {
          file.inProgress = false;
          return of(`${file.data.name} upload failed.`);
        })
      )
      .subscribe((event: any) => {
        if (typeof event === 'object') {
          file.path = event.body.path;
          file.mimetype = event.body.mimetype;
          file.inProgress = false;
        }
      });
  }
  uploadFiles(): void {
    this.tempFiles.forEach((file) => this.uploadFile(file));
    this.tempFiles = [];
  }

  fileUploadHandler(files: FileList): void {
    // tslint:disable-next-line: prefer-for-of
    for (let i = 0; i < files.length; i++) {
      const file = files[i];
      this.tempFiles.push({
        data: file,
        inProgress: true,
        progress: 0,
        name: file.name,
      });
    }
    this.uploadFiles();
  }
}
