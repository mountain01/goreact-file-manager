import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { File } from './file.entity';
import { Repository } from 'typeorm';

@Injectable()
export class FileService {
  constructor(@InjectRepository(File) private fileRepo: Repository<File>) {}

  getAllFiles() {
    return this.fileRepo.find();
  }

  create(file: File) {
    return this.fileRepo.save(file);
  }
}
