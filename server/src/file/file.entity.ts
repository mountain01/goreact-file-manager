import { PrimaryGeneratedColumn, Column, Entity } from 'typeorm';

@Entity()
export class File {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  destination: string;

  @Column()
  encoding: string;

  @Column()
  fieldname: string;

  @Column()
  filename: string;

  @Column()
  mimetype: string;

  @Column()
  originalname: string;

  @Column()
  path: string;

  @Column()
  size: number;
}
