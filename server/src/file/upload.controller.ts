import {
  Controller,
  Post,
  UseInterceptors,
  UploadedFile,
  Get,
  Param,
  Res,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { join } from 'path';
import { diskStorage } from 'multer';
import { FileService } from './file.service';
import { File } from './file.entity';

@Controller('uploads')
export class UploadController {
  constructor(private fileService: FileService) {}
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: 'uploads',
        filename(req, file, cb) {
          cb(null, new Date().toISOString() + '_' + file.originalname);
        },
      }),
    }),
  )
  uploadFile(@UploadedFile() file: File) {
    return this.fileService.create(file);
  }

  @Get(':image')
  getImage(@Param('image') image, @Res() response) {
    const path = join(__dirname, '../../', 'uploads', image);
    return response.sendFile(path);
  }

  @Get()
  getAllImages() {
    return this.fileService.getAllFiles();
  }
}
